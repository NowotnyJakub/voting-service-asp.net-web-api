﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommissionApp
{
    public class Vote
    {
        public string IdentificationNumber { get; set; }
        public string ChoosenCandidate { get; set; }
    }
}
