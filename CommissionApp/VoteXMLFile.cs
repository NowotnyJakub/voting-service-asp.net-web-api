﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace CommissionApp
{
    public class VoteXMLFile
    {
        static string xmlFile = Directory.GetCurrentDirectory();

        public static void SerializeAllVotes(List<Vote> votes)
        {
            using (StreamWriter file = new StreamWriter(xmlFile + "allVotes.xml"))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<Vote>));
                serializer.Serialize(file, votes);
                file.Close();
            }
        }

        public static List<Vote> DeserializeAllVotes()
        {
            List<Vote> votesList = new List<Vote>();
            XmlSerializer xs = new XmlSerializer(typeof(Vote[]));
            using (Stream ins = File.Open(xmlFile + "allVotes.xml", FileMode.Open))
                foreach (Vote singleCategory in (Vote[])xs.Deserialize(ins))
                    votesList.Add(singleCategory);

            return votesList;
        }
    }
}
