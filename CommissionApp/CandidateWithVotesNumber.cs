﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommissionApp
{
    public class CandidateWithVotesNumber
    {
        public string Name { get; set; }
        public int Result { get; set; }

        public CandidateWithVotesNumber(string name)
        {
            Name = name;
            Result = 0;
        }
    }
}
