﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CommissionApp
{
    class Program
    {
        static List<CandidateWithVotesNumber> candidateResults = new List<CandidateWithVotesNumber>();
        static List<Candidate> candidates = new List<Candidate>();
        static List<Vote> votes = new List<Vote>();

        static void Main(string[] args)
        {
            List<Vote> votesList = VoteXMLFile.DeserializeAllVotes();
            foreach (var item in votesList)
            {
                votes.Add(item);
            }

            while (true)
            {
                Console.WriteLine("Results app\n");

                GetResults().Wait();

                System.Threading.Thread.Sleep(5000);

                Console.Clear();
            }
        }

        static async Task GetResults()
        {
            candidateResults = new List<CandidateWithVotesNumber>();
            candidates = new List<Candidate>();
            bool alreadyVoted = false;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62827/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response;

                Console.WriteLine("Candidate results:");

                response = await client.GetAsync("api/Candidat/results");
                if (response.IsSuccessStatusCode)
                {
                    Candidate[] reports = await response.Content.ReadAsAsync<Candidate[]>();

                    candidates = reports.ToList();
                }

                foreach (var candidate in candidates)
                {
                    candidateResults.Add(new CandidateWithVotesNumber(candidate.Name));
                }
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62827/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response;

                response = await client.GetAsync("api/Vote/results");
                if (response.IsSuccessStatusCode)
                {
                    Vote[] reports = await response.Content.ReadAsAsync<Vote[]>();
                    foreach (var report in reports)
                    {

                        Console.WriteLine($"{report.IdentificationNumber}");

                        foreach (var vote in votes)
                        {
                            if (report.IdentificationNumber == vote.IdentificationNumber)
                            {
                                alreadyVoted = true;
                            }
                        }

                        if (!alreadyVoted)
                        {
                            votes.Add(report);
                        }
                    }
                }
                await client.DeleteAsync("api/Vote");
            }
            VoteXMLFile.SerializeAllVotes(votes);

            foreach (var vote in votes)
            {
                foreach (var candidate in candidateResults)
                {
                    if (candidate.Name == vote.ChoosenCandidate)
                    {
                        candidate.Result++;
                    }
                }
            }

            foreach (var candidate in candidateResults)
            {
                Console.WriteLine($"{candidate.Name} got: {candidate.Result} votes");
            }
        }
    }
}
