﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace VoteStationApp
{

    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {             
                GetCandidates().Wait();
                PostVote().Wait();
                Console.ReadKey();
                Console.Clear();
            }
        }

        static async Task GetCandidates()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62827/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response;

                Console.WriteLine("Candidates:");

                response = await client.GetAsync("api/Candidat/results");
                if (response.IsSuccessStatusCode)
                {
                    Candidate[] votes = await response.Content.ReadAsAsync<Candidate[]>();
                    foreach (var vote in votes)
                    {
                        Console.WriteLine($"{vote.Name}");
                    }
                }

            }


        }

        static async Task PostVote()
        {
            Vote newVote = new Vote();
            Console.WriteLine("Enter identification number:");
            newVote.IdentificationNumber = Console.ReadLine();
            Console.WriteLine("Enter choosen candidate:");
            newVote.ChoosenCandidate = Console.ReadLine();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62827/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.PostAsJsonAsync("api/Vote", newVote);

                if (response.IsSuccessStatusCode)
                {
                    bool result = await response.Content.ReadAsAsync<bool>();
                    if (result)
                        Console.WriteLine("Vote Added");
                    else
                        Console.WriteLine("Incorrect data");
                }
            }
        }
    }
}