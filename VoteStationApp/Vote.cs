﻿namespace VoteStationApp
{
    public class Vote
    {
        public string IdentificationNumber { get; set; }
        public string ChoosenCandidate { get; set; }
    }
}