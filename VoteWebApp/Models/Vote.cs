﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoteWebApp.Models
{
    public class Vote
    {
        public string IdentificationNumber { get; set; }
        public string ChoosenCandidate { get; set; }
    }
}