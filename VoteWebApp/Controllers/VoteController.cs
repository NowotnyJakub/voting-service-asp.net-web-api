﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VoteWebApp.Models;

namespace VoteWebApp.Controllers
{
    public class VoteController : ApiController
    {
        public static List<Vote> votes = new List<Vote>();

        [Route("api/Vote/results")]
        [HttpGet]
        public List<Vote> Get()
        {
            return votes;
        }

        [HttpPost]
        public bool Post(Vote newVote)
        {
            if (newVote.IdentificationNumber.Length != 11)
            {
                return false;
            }
            votes.Add(newVote);
            return true;
        }

        public void Delete()
        {
            votes = new List<Vote>();
        }
    }
}
