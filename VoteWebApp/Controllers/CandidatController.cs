﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VoteWebApp.Models;

namespace VoteWebApp.Controllers
{
    public class CandidatController : ApiController
    {
        public static List<Candidat> candidates = new List<Candidat>()
        {
            new Candidat {Name = "Walesa"},
            new Candidat {Name = "Komorowski"},
            new Candidat {Name = "Duda"},
        };

        [Route("api/Candidat/results")]
        [HttpGet]
        public List<Candidat> Get()
        {
            return candidates;
        }
    }
}

